package productsInShopRun;


import java.math.BigInteger;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import productsInShopJAXB.Category;
import productsInShopJAXB.Product;
import productsInShopJAXB.Products;
import productsInShopJAXB.Subcategory;



/**
 * This class creates objects JAXB classes
 * 
 * @author Vladislav_Danilov
 *
 */
public class CreatorElement {

  public Category createCategory(String nameCategory, List<Subcategory> subcategory) {
    Category category = new Category();
    category.nameCategory = nameCategory;
    category.subcategory = subcategory;
    return category;
  }

  public Subcategory createSubcategory(String nameCategory, List<Product> product) {
    Subcategory subCategory = new Subcategory();
    subCategory.nameCategory = nameCategory;
    subCategory.product = product;
    return subCategory;
  }

  public Product createProduct(String nameProduct, LocalDate date, String model, double price,
      BigInteger quantity, String color) {
    Product product = new Product();
    product.setNameProduct(nameProduct);
    /*�������������� LocalDate � XMLGregorianCalendar*/
    GregorianCalendar gcal = GregorianCalendar.from(date.atStartOfDay(ZoneId.systemDefault()));
    XMLGregorianCalendar xcal = null;
    try {
      xcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
    } catch (DatatypeConfigurationException e) {
      e.printStackTrace();
    }
    product.setDate(xcal);
    product.setModel(model);
    product.setColor(color);
    product.setPrice(price);
    product.setQuantity(quantity);
    return product;
  }

  public Products createProducts(List<Category> category) {
    Products products = new Products();
    products.category = category;
    return products;
  }

}
