package productsInShopRun;

import java.io.StringWriter;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.namespace.QName;
import productsInShopJAXB.Category;
import productsInShopJAXB.Product;
import productsInShopJAXB.Products;
import productsInShopJAXB.Subcategory;

public class Main {

  public static void main(String[] args) {
    Products products;
    List<Category> category = new ArrayList<Category>();
    List<Subcategory> subcategory = new ArrayList<Subcategory>();
    List<Product> product = new ArrayList<Product>();

    CreatorElement creator = new CreatorElement();

    product.add(creator.createProduct("Продукт1", LocalDate.of(2014, Month.DECEMBER, 12), "qw33",
        2000.00, BigInteger.valueOf(4), "#acacac"));
    product.add(creator.createProduct("Продукт2", LocalDate.of(2014, Month.DECEMBER, 12), "qw43",
        2000.00, BigInteger.valueOf(4), "#acac22"));

    subcategory.add(creator.createSubcategory("Подкатегория 1", product));

    category.add(creator.createCategory("Категория 1", subcategory));

    products = creator.createProducts(category);

    QName root = new QName("products");
    JAXBElement<Products> je = new JAXBElement<Products>(root, Products.class, products);
    StringWriter writer = null;
    try {
      JAXBContext jc = JAXBContext.newInstance(Products.class);
      Marshaller marshaller = jc.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      writer = new StringWriter();
      marshaller.marshal(je, writer);
    } catch (PropertyException e) {
      e.printStackTrace();
    }catch (JAXBException e) {
      e.printStackTrace();
    }
    System.out.println(writer.toString());
  }

}
